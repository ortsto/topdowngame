// Fill out your copyright notice in the Description page of Project Settings.

#include "Practica1.h"
#include "VBala.h"
#include "Tower.h"


// Sets default values
ATower::ATower()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	rot_ = FRotator(0, 0, 0);
	imShooting = false;
	wasDamaged = false;
	score_ = 0;
	life_ = 1.0f;
	BoxColl = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = BoxColl;
	BoxColl->SetWorldScale3D(FVector(5, 10, 2));
	BoxColl->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlapBegin);
	BoxColl->OnComponentEndOverlap.AddDynamic(this, &ATower::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATower::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (life_ < 0) {
		UGameplayStatics::OpenLevel(this, "Start");
	}
}

// Called to bind functionality to input
void ATower::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("RotationTower", this, &ATower::RotationTower);
	//InputComponent->BindAction("ResetVR", IE_Pressed, this, &ATower::RotationTower);
	InputComponent->BindAction("Shooting", EInputEvent::IE_Pressed, this, &ATower::Shooting);
	InputComponent->BindAction("Shooting", EInputEvent::IE_Released, this, &ATower::NoShooting);
}

void ATower::RotationTower(float rate) {

	FRotator tmpRot = FRotator(0, rate, 0);
	FRotator tmpRotation =  GetActorRotation();
	
	
	if (tmpRotation.Yaw < -80.0f || tmpRotation.Yaw > +80.0f) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, tmpRotation.ToString());
		//FRotator solution = frota
		SetActorRotation(FRotator(0, 79*rate, 0));
	}
	else {
		AddActorLocalRotation(tmpRot);
	}
	
}

void ATower::Shooting() {
	imShooting = true;
}

void ATower::NoShooting() {
	imShooting = false;
}

void ATower::updateScore() {
	score_ += 10;
}
void ATower::updateLife() {
	life_ -= 0.1f;
}

void ATower::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		OtherActor->Destroy();
		updateLife();
		wasDamaged = true;
	}
}

void ATower::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}