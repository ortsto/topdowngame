// Fill out your copyright notice in the Description page of Project Settings.

#include "Practica1.h"
#include "EnemyBase.h"
#include "Tower.h"
#include "VBala.h"


// Sets default values
AVBala::AVBala()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = Mesh;
	balaMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BalaMovement"));

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("SphereColl"));
	sphere->InitSphereRadius(60.0f);
	sphere->SetupAttachment(Mesh);
	sphere->OnComponentBeginOverlap.AddDynamic(this, &AVBala::OnOverlapBegin);
	sphere->OnComponentEndOverlap.AddDynamic(this, &AVBala::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AVBala::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(handleTimer, this, &AVBala::DeleteActorTimer, 3.0f, false);
}

// Called every frame
void AVBala::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (balaMovement->Velocity.X == 0 && balaMovement->Velocity.Y == 0 && balaMovement->Velocity.Z == 0) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "Se paro");
		this->Destroy();
	}
}
void AVBala::DeleteActorTimer() {
	this->Destroy();
}

void AVBala::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//OtherActor->Destroy();
		ATower* player = Cast<ATower>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			AEnemyBase * tmpBala = Cast<AEnemyBase>(*ActorItr);
			if (tmpBala == OtherActor) {
				//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "ES BALA");
				OtherActor->Destroy();
				player->updateScore();
				this->Destroy();

			}
		}
	}
}

void AVBala::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}