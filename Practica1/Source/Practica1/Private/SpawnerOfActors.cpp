// Fill out your copyright notice in the Description page of Project Settings.

#include "Practica1.h"
#include "EnemyBase.h"
#include "SpawnerOfActors.h"


// Sets default values
ASpawnerOfActors::ASpawnerOfActors()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	EnemySpawnCount = 0;
	
}

// Called when the game starts or when spawned
void ASpawnerOfActors::BeginPlay()
{
	Super::BeginPlay();
	restartValue = FMath::RandRange(10, 20);
	//tmpBase->position_to = FVector(0, 0, 0);
	GetWorldTimerManager().SetTimer(timer, this, &ASpawnerOfActors::spawnActorBy, 1.0f, true);
}

// Called every frame
void ASpawnerOfActors::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	//FRotator Rotation(0.0f, 0.0f, 0.0f);
	

	//FActorSpawnParameters SpawnInfo;
	//AEnemyBase*  tmpBase = GetWorld()->SpawnActor<AEnemyBase>(myEnemyPrefab, this->GetActorLocation(), FRotator::ZeroRotator, SpawnInfo);
}

FVector ASpawnerOfActors::returnPosition() {
	FVector tmpPositon;

	tmpPositon.X = -1800;
	tmpPositon.Y = FMath::RandRange(-600, 600);
	tmpPositon.Z = 100;

	return tmpPositon;

}

bool ASpawnerOfActors::updateCount() {
	EnemySpawnCount++;
	if (EnemySpawnCount > restartValue) {
		EnemySpawnCount = 0;
		return true;
	}
	return false;
}

void ASpawnerOfActors::spawnActorBy() {
	
	
	if (EnemySpawnCount > 15) {
		FActorSpawnParameters SpawnInfo;
		APickup_DestroyAll * tmpPickup = GetWorld()->SpawnActor<APickup_DestroyAll>(myPickupPrefab, this->GetActorLocation(), FRotator::ZeroRotator, SpawnInfo);
		if(tmpPickup != nullptr) {
			tmpPickup->position_to = returnPosition();
		}
		EnemySpawnCount = 0;
	}
	else {
		FActorSpawnParameters SpawnInfo;
		AEnemyBase*  tmpBase = GetWorld()->SpawnActor<AEnemyBase>(myEnemyPrefab, this->GetActorLocation(), FRotator::ZeroRotator, SpawnInfo);
		tmpBase->position_to = returnPosition();
	}

	EnemySpawnCount++;
}