// Fill out your copyright notice in the Description page of Project Settings.

#include "Practica1.h"
#include "EnemyBase.h"
#include "VBala.h"
#include "Tower.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"


// Sets default values
AEnemyBase::AEnemyBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// set our turn rates for input
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = Mesh;
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxComponent->SetupAttachment(Mesh);
	position_to= FVector(0,0,0);
	

	
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyBase::OnOverlapBegin);     
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &AEnemyBase::OnOverlapEnd);
	//this->Tags.Add(FName("Bala"));
}

// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	if (position_to.X == 0 && position_to.Y == 0 && position_to.Z == 0) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "SIN INICIALIZAR");
	}
}

// Called every frame
void AEnemyBase::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	//FMath::RandRange(1.5f, 5.0f);
	UKismetSystemLibrary::MoveComponentTo(RootComponent, position_to, FRotator(0.0f, 0.0f, 0.0f), false, false, FMath::RandRange(1.5f, 5.0f),false, EMoveComponentAction::Type::Move, LatentInfo);
}

void AEnemyBase::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{

	

		ATower* player = Cast<ATower>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		//auto myAct = Cast<AVBala>(OtherActor);
		if (player == OtherActor) {
		//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "ES PLAYER");
		}
		else
		{
			for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				AVBala * tmpBala = Cast<AVBala>(*ActorItr);
				if (tmpBala == OtherActor) {
					//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "ES BALA");
					player->updateScore();
					tmpBala->Destroy();
					this->Destroy();
				}
			}
		}
	}
}

void AEnemyBase::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}

void AEnemyBase::whenPickup() {
	ATower* player = Cast<ATower>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	player->updateScore();
	this->Destroy();
}