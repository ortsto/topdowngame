// Fill out your copyright notice in the Description page of Project Settings.

#include "Practica1.h"
#include "VBala.h"
#include "EnemyBase.h"
#include "Tower.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Pickup_DestroyAll.h"


// Sets default values
APickup_DestroyAll::APickup_DestroyAll()
{
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	
	//RootComponent = Mesh;
	BoxComponent->SetupAttachment(Mesh);
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	position_to = FVector(0, 0, 0);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &APickup_DestroyAll::OnOverlapBegin);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &APickup_DestroyAll::OnOverlapEnd);

}

// Called when the game starts or when spawned
void APickup_DestroyAll::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup_DestroyAll::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	UKismetSystemLibrary::MoveComponentTo(RootComponent, position_to, FRotator(0.0f, 0.0f, 0.0f), false, false, FMath::RandRange(1.5f, 5.0f), false, EMoveComponentAction::Type::Move, LatentInfo);

}

void APickup_DestroyAll::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{

		ATower* player = Cast<ATower>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		//auto myAct = Cast<AVBala>(OtherActor);
		if (player == OtherActor) {
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "ES PLAYER");
		}
		else
		{
			for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				AVBala * tmpBala = Cast<AVBala>(*ActorItr);
				if (tmpBala == OtherActor) {
					for (TActorIterator<AActor> ActorItr2(GetWorld()); ActorItr2; ++ActorItr2)
					{
						AEnemyBase * tmpEnemy = Cast<AEnemyBase>(*ActorItr2);
						if (tmpEnemy) {
							tmpEnemy->whenPickup();
						}
					}
					//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "ES BALA");
					//player->updateScore();
					tmpBala->Destroy();
					this->Destroy();
				}
			}
		}
	}
}

void APickup_DestroyAll::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}