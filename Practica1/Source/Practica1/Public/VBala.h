// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "VBala.generated.h"

UCLASS()
class PRACTICA1_API AVBala : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVBala();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement Component")
		class UProjectileMovementComponent * balaMovement;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
		class UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision Components")
		class USphereComponent  * sphere;
	FTimerHandle handleTimer;
	void DeleteActorTimer();

	//ADDING SAME METHODS TO VBALA
	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void OnOverlapEnd_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
