// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Tower.generated.h"

UCLASS()
class PRACTICA1_API ATower : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATower();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void RotationTower(float rate);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision Components")
	class UBoxComponent * BoxColl;

	void Shooting();
	void NoShooting();
	FRotator rot_;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Shoot)
	bool imShooting;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Score)
	int score_;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Life)
	float life_;
	UFUNCTION(BlueprintCallable, Category = "Score")
		void updateScore();
	UFUNCTION(BlueprintCallable, Category = "Life")
		void updateLife();

	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void OnOverlapEnd_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShakeCamera")
		bool wasDamaged;
};
