// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EnemyBase.h"
#include "Pickup_DestroyAll.h"
#include "SpawnerOfActors.generated.h"

UCLASS()
class PRACTICA1_API ASpawnerOfActors : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnerOfActors();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	UFUNCTION(BlueprintCallable, Category = "NewPosition")
	FVector returnPosition();
	UFUNCTION(BlueprintCallable, Category = "CountSpawner")
	bool  updateCount();
	int restartValue;
	int EnemySpawnCount;
	FTimerHandle timer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		TSubclassOf<AEnemyBase> myEnemyPrefab;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		TSubclassOf<APickup_DestroyAll> myPickupPrefab;
	UFUNCTION()
	void spawnActorBy();
};
